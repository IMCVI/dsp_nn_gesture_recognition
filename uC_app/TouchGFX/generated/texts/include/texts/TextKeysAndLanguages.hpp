/* DO NOT EDIT THIS FILE */
/* This file is autogenerated by the text-database code generator */

#ifndef TOUCHGFX_TEXTKEYSANDLANGUAGES_HPP
#define TOUCHGFX_TEXTKEYSANDLANGUAGES_HPP

enum LANGUAGES
{
    GB,
    NUMBER_OF_LANGUAGES
};

enum TEXTS
{
    T___SINGLEUSE_9ZGY,
    T___SINGLEUSE_NLJU,
    T___SINGLEUSE_5KAH,
    T___SINGLEUSE_SGIW,
    T___SINGLEUSE_YFE4,
    T___SINGLEUSE_0W2C,
    T___SINGLEUSE_HU1N,
    T___SINGLEUSE_BMOA,
    T___SINGLEUSE_T9B5,
    T___SINGLEUSE_UQ5K,
    T___SINGLEUSE_XJCH,
    T___SINGLEUSE_93ZG,
    T___SINGLEUSE_DT4C,
    T___SINGLEUSE_QFL5,
    T___SINGLEUSE_2B5P,
    T___SINGLEUSE_IEB1,
    NUMBER_OF_TEXT_KEYS
};

#endif // TOUCHGFX_TEXTKEYSANDLANGUAGES_HPP
