/*********************************************************************************/
/********** THIS FILE IS GENERATED BY TOUCHGFX DESIGNER, DO NOT MODIFY ***********/
/*********************************************************************************/
#ifndef PLOTVIEWBASE_HPP
#define PLOTVIEWBASE_HPP

#include <gui/common/FrontendApplication.hpp>
#include <mvp/View.hpp>
#include <gui/plot_screen/PlotPresenter.hpp>
#include <touchgfx/widgets/Box.hpp>
#include <touchgfx/widgets/graph/GraphScroll.hpp>
#include <touchgfx/widgets/graph/GraphElements.hpp>
#include <touchgfx/widgets/canvas/PainterRGB565.hpp>
#include <touchgfx/widgets/graph/GraphLabels.hpp>
#include <touchgfx/widgets/Button.hpp>
#include <touchgfx/widgets/TextArea.hpp>

class PlotViewBase : public touchgfx::View<PlotPresenter>
{
public:
    PlotViewBase();
    virtual ~PlotViewBase() {}
    virtual void setupScreen();

protected:
    FrontendApplication& application() {
        return *static_cast<FrontendApplication*>(touchgfx::Application::getInstance());
    }

    /*
     * Member Declarations
     */
    touchgfx::Box __background;
    touchgfx::Box box1;
    touchgfx::GraphScroll<100> dynamicGraph1;
    touchgfx::GraphElementLine dynamicGraph1Line1;
    touchgfx::PainterRGB565 dynamicGraph1Line1Painter;
    touchgfx::GraphElementGridY dynamicGraph1MajorYAxisGrid;
    touchgfx::GraphLabelsY dynamicGraph1MajorYAxisLabel;
    touchgfx::GraphScroll<100> dynamicGraph2;
    touchgfx::GraphElementLine dynamicGraph2Line1;
    touchgfx::PainterRGB565 dynamicGraph2Line1Painter;
    touchgfx::GraphElementGridY dynamicGraph2MajorYAxisGrid;
    touchgfx::GraphLabelsY dynamicGraph2MajorYAxisLabel;
    touchgfx::GraphScroll<100> dynamicGraph3;
    touchgfx::GraphElementLine dynamicGraph3Line1;
    touchgfx::PainterRGB565 dynamicGraph3Line1Painter;
    touchgfx::GraphElementGridY dynamicGraph3MajorYAxisGrid;
    touchgfx::GraphLabelsY dynamicGraph3MajorYAxisLabel;
    touchgfx::Button Rtn;
    touchgfx::TextArea textArea1;
    touchgfx::TextArea textArea1_1;
    touchgfx::TextArea textArea1_2;

private:

    /*
     * Callback Declarations
     */
    touchgfx::Callback<PlotViewBase, const touchgfx::AbstractButton&> buttonCallback;

    /*
     * Callback Handler Declarations
     */
    void buttonCallbackHandler(const touchgfx::AbstractButton& src);

    /*
     * Canvas Buffer Size
     */
    static const uint16_t CANVAS_BUFFER_SIZE = 4800;
    uint8_t canvasBuffer[CANVAS_BUFFER_SIZE];
};

#endif // PLOTVIEWBASE_HPP
