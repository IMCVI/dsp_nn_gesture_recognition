#ifndef MODEL_HPP
#define MODEL_HPP


class ModelListener;

class Model
{
public:
    Model();

    void bind(ModelListener* listener)
    {
        modelListener = listener;
    }

    void tick();
    void update_angXval(float val);
    void update_angYval(float val);
    void update_angZval(float val);
    float update_angXval();
    float update_angYval();
    float update_angZval();
protected:
    int tickCounter;
    ModelListener* modelListener;
};

#endif // MODEL_HPP
