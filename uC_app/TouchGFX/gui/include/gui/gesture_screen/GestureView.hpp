#ifndef GESTUREVIEW_HPP
#define GESTUREVIEW_HPP

#include <gui_generated/gesture_screen/GestureViewBase.hpp>
#include <gui/gesture_screen/GesturePresenter.hpp>

class GestureView : public GestureViewBase
{
public:
    GestureView();
    virtual ~GestureView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // GESTUREVIEW_HPP
