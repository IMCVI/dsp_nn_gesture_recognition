#ifndef PLOTPRESENTER_HPP
#define PLOTPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class PlotView;

class PlotPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    PlotPresenter(PlotView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~PlotPresenter() {};
    virtual void update_angXval(float val);
    virtual void update_angYval(float val);
    virtual void update_angZval(float val);
    //void setAngV_Xval(float val);

private:
    PlotPresenter();

    PlotView& view;
};

#endif // PLOTPRESENTER_HPP
