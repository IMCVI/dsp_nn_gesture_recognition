#ifndef PLOTVIEW_HPP
#define PLOTVIEW_HPP

#include <gui_generated/plot_screen/PlotViewBase.hpp>
#include <gui/plot_screen/PlotPresenter.hpp>
#include "L3GD20.h"

class PlotView : public PlotViewBase
{
public:
    PlotView();
    virtual ~PlotView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
    void handleTickEvent();
    void update_angXval(float val);
    void update_angYval(float val);
    void update_angZval(float val);
    void setAngV_Xval(float val);
protected:
    int tickCounter;
    int lastVal;
};

#endif // PLOTVIEW_HPP
