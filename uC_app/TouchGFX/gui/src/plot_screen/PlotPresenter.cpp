#include <gui/plot_screen/PlotView.hpp>
#include <gui/plot_screen/PlotPresenter.hpp>

PlotPresenter::PlotPresenter(PlotView& v)
    : view(v)
{

}

void PlotPresenter::activate()
{

}

void PlotPresenter::deactivate()
{

}

void PlotPresenter::update_angXval(float val)
{
	view.update_angXval(val);
}

void PlotPresenter::update_angYval(float val)
{
	view.update_angYval(val);
}

void PlotPresenter::update_angZval(float val)
{
	view.update_angZval(val);
}
