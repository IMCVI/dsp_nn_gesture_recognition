#include <gui/plot_screen/PlotView.hpp>

PlotView::PlotView()
{
	tickCounter = 0;
}

void PlotView::setupScreen()
{
    PlotViewBase::setupScreen();
}

void PlotView::tearDownScreen()
{
    PlotViewBase::tearDownScreen();
}


void PlotView::handleTickEvent()
{
	tickCounter++;

	if (tickCounter % 3 ==0)
	{
		//dynamicGraph1.addDataPoint(100);
		//dynamicGraph1.addDataPoint((int)LastAngleRate_X); ///EEEEEEEERRRRRRRRRRRRRRROR
		//dynamicGraph1.addDataPoint((int)val);
	}
}

void PlotView::update_angXval(float val)
{
	//if(lastVal != val)
	//{
	//	lastVal = (int)val;
		dynamicGraph1.addDataPoint((int)val);
	//}
}

void PlotView::update_angYval(float val)
{
	//if(lastVal != val)
	//{
	//	lastVal = (int)val;
		dynamicGraph2.addDataPoint((int)val);
	//}
}

void PlotView::update_angZval(float val)
{
	//if(lastVal != val)
	//{
	//	lastVal = (int)val;
		dynamicGraph3.addDataPoint((int)val);
	//}
}
