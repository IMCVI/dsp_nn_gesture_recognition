#include <gui/model/Model.hpp>
#include <gui/model/ModelListener.hpp>
#include "main.h"

//extern

Model::Model() : modelListener(0)
{
	tickCounter = 0;
}

void Model::tick()
{
	tickCounter++;
	if((tickCounter % 3) == 0 )
	{
		//if(modelListener != 0)
		//{
			modelListener->update_angXval(update_angXval());
		//}

		//if(modelListener != 0)
		//{
			modelListener->update_angYval(update_angYval());
		//}

		//if(modelListener != 0)
		//{
			modelListener->update_angZval(update_angZval());
		//}
	}
}


float Model::update_angXval()
{
	return angV_getValue('x');
}

float Model::update_angYval()
{
	return angV_getValue('y');
}

float Model::update_angZval()
{
	return angV_getValue('z');
}
